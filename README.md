# WooCommerce settings

##General tab
* General Options -> Base Location : Cyprus
* General Options -> Selling Location(s) : Sell to All Countries
* General Options -> Shipping Location(s) : Disable shipping & shipping calculations
* General Options -> Default Customer Location : Geolocate
* General Options -> Enable Taxes : Uncheck

##Products tab

###Display Section

* Shop & Product Pages -> Add to cart behavior ->  Redirect to the cart page after successful addition : Check
* Shop & Product Pages -> Add to cart behavior ->  Enable AJAX add to cart buttons on archives : Un-Check

###Inventory Section

* Inventory -> Enable stock management : Uncheck
* Inventory -> Hold stock (minutes) : Empty

##Checkout tab

###Checkout Options Section

* Checkout Process -> Coupons -> Enable the use of coupons : Checked
* Checkout Process -> Checkout Process -> Enable guest checkout : Unchecked

* Checkout Pages -> Cart Page -> Checkout
* Checkout Pages -> Checkout Page -> Checkout
* Checkout Pages -> Terms and Conditions -> Empty

* Checkout Endpoints : Leave all default values

* Payment Gateways : Paypal

###Paypal Section

* Paypal -> Enable/Disable: Check
* Paypal -> Title -> Paypal
* Paypal -> Description : Empty
* Paypal -> PayPal Email: The paypal email ID of the company
* Paypal -> Paypal Sandbox : Uncheck
* Paypal -> Debug Log: Check

* Advanced options -> Invoice Prefix: TPS-
* Advanced options -> Payment Action : Capture

* API Credentials -> API Username
* API Credentials -> API Password
* API Credentials -> API Signature

API credentials are required to process automatic refunds via PayPal. Read below how to find API credentials.

## Accounts tab

* Account Pages -> Enable registration -> Enable registration on the "Checkout" page: Checked

* Account Pages -> Enable registration -> Enable registration on the "My Account" page: Checked
* Account Pages -> Login -> Display returning customer login reminder on the "Checkout" page: Checked

* Account Pages -> Account Creation -> Automatically generate username from customer email : Checked
* Account Pages -> Account Creation -> Automatically generate customer password  : Un-Checked

* My Account Endpoints: Orders, View Order, Edit Account, Addresses, Lost password, Logout

## Emails tab

*Disable "New account" email notification

Enable all emails apart from the New account email notification.

* Email Sender Options -> "From" Name : The Parents Shop
* Email Sender Options -> "From" Address : sales@theparentsshop.com

* Email Template -> Header Image : http://www.theparentsshop.com/cms/files/2016/11/logo-menu-theparentsshop.png
* Email Template -> Footer Text : The Parents Shop

* Email Template -> Base Colour : #ffffff
* Email Template -> Background Colour : #f8f8f8
* Email Template -> Body Background Colour : #ffffff
* Email Template -> Body Text Colour : #000000

# Paypal configuration

https://docs.woocommerce.com/document/paypal-standard/

## PayPal IPN URL
In your PayPal Business account, go to: Profile > Profile and settings > My selling tools. Click on Instant payment notifications to set your URL. Use the following, replacing example.com with your own URL:

* https://www.theparentsshop.com/?wc-api=WC_Gateway_Paypal

## Auto-Return

You can set up auto-return in your PayPal account, which will take customers to a receipt page. This setting is in "My selling tools-> Website preferences" menu. For example, use the following URL and replace example.com with your own URL:

* https://www.theparentsshop.com/checkout/order-received/

## API Credentials
* Login to PayPal
* Go to Profile > Profile and Settings
* Click My Selling Tools
* Go to API Access
* Select NVP/SOAP API integration
* Click to view API Username, Password and Signature
* Add information into WooCommerce PayPal settings

# Testing

* Ensure that when a visitor tries to register with an already existent email, it does not throw a 404 page, but it properly reloads page with the corresponding warning.
* Ensure that the Orders & Addresses Endpoints are only shown to Customers ( with 'customer' role )
* Test Paypal IPN on demo site

# TO DO
* Update Email Template logo with the value from s3 logo