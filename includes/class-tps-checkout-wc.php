<?php

/**
* Tps_Checkout_Wc Class
 */
class Tps_Checkout_WC {

	public static function empty_cart_before_add_product( $cart_item_data ) {
		
		global $woocommerce;
	    $woocommerce->cart->empty_cart();

	    // Do nothing with the data and return
	    return $cart_item_data; 
	}

	public static function override_default_checkout_fields( $fields ) {

     	$fields['order']['order_comments']['label'] = __( 'Product description' , 'tps-checkout' );
     	$fields['order']['order_comments']['placeholder'] = __( 'Write a short description of your product' , 'tps-checkout' );
     	$fields['order']['order_comments']['required'] = true;

	    unset($fields['billing']['billing_phone']);
	    unset($fields['billing']['billing_country']);
	    unset($fields['billing']['billing_address_1']);
	    unset($fields['billing']['billing_address_2']);
	    unset($fields['billing']['billing_city']);
	    unset($fields['billing']['billing_state']);
	    unset($fields['billing']['billing_postcode']);

	    return $fields;
	}

	public static function add_tps_checkout_fields( $checkout ){

		echo '<div id="order_product_link_checkout_field">';

	    woocommerce_form_field( '_tps_order_product_link', array(
	        'type'          => 'text',
	        'required'      => true,
	        'class'         => array('_tps_order_product_link'),
	        'label'         => __( 'Product Link' , 'tps-checkout' ),
	        'placeholder'   => __( 'E.g. http://www.mycompany.com/myproduct' , 'tps-checkout' ),
	        ), $checkout->get_value( '_tps_order_product_link' ));

    	echo '</div>';

	}

	public static function validate_tps_checkout_fields() {
	    
	    // Product link is required
	    if ( ! $_POST['_tps_order_product_link'] ){
	        wc_add_notice( __( 'Please enter a Product Link.' ), 'error' );
		}

	    // Valid url is required
	    if ( filter_var( $_POST['_tps_order_product_link'] , FILTER_VALIDATE_URL) === false ){
	        wc_add_notice( __( 'Please enter a valid product URL (http:// or https:// is required).' ), 'error' );
		}

	}

	public static function tps_checkout_fields_update_order_meta( $order_id ) {
	    
	    if ( ! empty( $_POST['_tps_order_product_link'] ) ) {
	        update_post_meta( $order_id, '_tps_order_product_link' , esc_url_raw ( $_POST['_tps_order_product_link'] ) );
	    }
	}

	public static function tps_checkout_fields_display_admin_order_meta($order){
	    
	    echo '<p><strong>'.__( 'Product Link' , 'tps-checkout' ).':</strong> ' . get_post_meta( $order->id, '_tps_order_product_link', true ) . '</p>';
	}


	public static function tps_checkout_fields_display_in_emails( $fields, $sent_to_admin, $order ) {

		$fields['_tps_order_product_link']= array();
		$fields['_tps_order_product_link']['label']= __( 'Product Link' , 'tps-checkout' );
		$fields['_tps_order_product_link']['value']= get_post_meta( $order->id, '_tps_order_product_link', true );

		return $fields;
	}

	public static function change_customer_role_back_to_default( $order_id ){
		$order = wc_get_order( $order_id );
		$user = new WP_User( $order->user_id );
		//$user->remove_role( 'subscriber' );
		$user -> add_role( 'customer' );
	}

	public static function paypal_icon( $icon_html, $id ){

		$icon_html='<i class="fa fa-paypal fa-2x"></i>';

		return $icon_html;
	}

	public static function remove_woocommerce_orders_endpoint_from_my_account( $items ){

		$user = wp_get_current_user();

		$user_is_customer = false;

		foreach ( $user->roles as $key => $role){
			
			if ($role == 'customer'){
				$user_is_customer = true;
			}
		}

		if ( !$user_is_customer ){
			unset( $items['orders'] );
			unset( $items['edit-address'] );
		}

		return $items;
	}

}