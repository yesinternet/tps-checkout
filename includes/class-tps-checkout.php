<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Checkout
 * @subpackage Tps_Checkout/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Tps_Checkout
 * @subpackage Tps_Checkout/includes
 * @author     Haris Kaklamanos <ckaklamanos@theparentsshop.com>
 */
class Tps_Checkout {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Tps_Checkout_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'tps-checkout';
		$this->version = '1.0.1';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Tps_Checkout_Loader. Orchestrates the hooks of the plugin.
	 * - Tps_Checkout_i18n. Defines internationalization functionality.
	 * - Tps_Checkout_Admin. Defines all hooks for the admin area.
	 * - Tps_Checkout_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for WC actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-checkout-wc.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-checkout-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-checkout-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-tps-checkout-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-tps-checkout-public.php';

		$this->loader = new Tps_Checkout_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Tps_Checkout_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Tps_Checkout_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Tps_Checkout_Admin( $this->get_plugin_name(), $this->get_version() );

		//$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		//$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Tps_Checkout_Public( $this->get_plugin_name(), $this->get_version() );

		//$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		//$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		//Empty cart before adding a product to it,
		//so that always there is only 1 product in the cart
		$this->loader->add_filter( 'woocommerce_add_cart_item_data', 'Tps_Checkout_WC', 'empty_cart_before_add_product' );
		
		//Override/remove several default checkout fields
		$this->loader->add_filter( 'woocommerce_checkout_fields', 'Tps_Checkout_WC', 'override_default_checkout_fields' );
		
		//Add custom checkout fields
		$this->loader->add_filter( 'woocommerce_after_order_notes', 'Tps_Checkout_WC', 'add_tps_checkout_fields' );
		$this->loader->add_action( 'woocommerce_checkout_process', 'Tps_Checkout_WC', 'validate_tps_checkout_fields' );
		$this->loader->add_action( 'woocommerce_checkout_update_order_meta', 'Tps_Checkout_WC', 'tps_checkout_fields_update_order_meta' );
		$this->loader->add_action( 'woocommerce_admin_order_data_after_billing_address', 'Tps_Checkout_WC', 'tps_checkout_fields_display_admin_order_meta', 10, 1 );
		$this->loader->add_filter( 'woocommerce_email_customer_details_fields', 'Tps_Checkout_WC', 'tps_checkout_fields_display_in_emails', 10, 3 );

		// Change user role back to 'customer' if an order is created.
		// This will differentiate them from subscribers
		$this->loader->add_action( 'woocommerce_new_order', 'Tps_Checkout_WC', 'change_customer_role_back_to_default' );

		//Modify Paypal Acceptance mark on checkout page
		$this->loader->add_filter( 'woocommerce_gateway_icon', 'Tps_Checkout_WC', 'paypal_icon', 10 , 2 );

		// Remove Orders My Account Endpoint for all users apart from customers
		$this->loader->add_filter( 'woocommerce_account_menu_items', 'Tps_Checkout_WC', 'remove_woocommerce_orders_endpoint_from_my_account', 10, 1 ) ;
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Tps_Checkout_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
